import 'package:flutter/material.dart';
import './pages/home.dart';
import './pages/login.dart';

void main() => runApp(
  new MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Login(),
  ),
);
